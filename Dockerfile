FROM telegraf:latest

RUN curl -s https://packagecloud.io/install/repositories/ookla/speedtest-cli/script.deb.sh | bash

RUN DEBIAN_FRONTEND=noninteractive apt install speedtest

COPY ./exec-speedtest.conf /etc/telegraf/telegraf.d/exec-speedtest.conf

COPY ./telegraf.conf /etc/telegraf/telegraf.d/telegraf.conf